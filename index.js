// console.log("s20 index.js Test");

// [SECTION] While loop

/*
	- A while loop takes in an axpression/condition
	- Expressions are any unit of code that can be evalauted to  avlue
	- If the condition evalutes to true, the statements inside the block will be executed.

*/


let count = 5;

while (count !== 0) {
	console.log("Count: " + count);
	count--; //decrements count value
}


// [SECTION] Do while

/*let number = parseInt(prompt("Give me a number: "));
// The "Number" function works similar to the "parseInt" function.


do {
	console.log("Number: " + number);
	number++; //increments value by 1

} while (number < 10); //number must be less than 10*/

/*
let n = 15;

while (number < 10) {
	conosle.log("Number: " + number);
	number++;
}

*/


// [SECTION] For loop
// initialization // condition // change of value
for (let count =0; count <=20; count++) {
	console.log(count);
}

let myString = "tupe";
// t = index 0
// u = index 1
// p = index 2
// e = index 3
console.log("myString length: " + myString.length);

/*console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);*/

// initialization
for (i = 0; i < myString.length; i++) {
	console.log(myString[i]);
}

let myName = "AlEx";
let vowelCount = 0;
for (let i = 0; i < myName.length; i++) {
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		) {
		console.log(3);
		vowelCount++;
	} else {
		console.log(myName[i]);
	}
}

console.log("Vowels: " + vowelCount);